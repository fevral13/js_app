(ns js-app.events.phone-numbers
  (:require [js-app.helpers :as helpers]
            [re-frame.core :as re-frame]
            [ajax.core :as ajax]
            [cljs.core.async :refer [>! close!]])
  (:require-macros [cljs.core.async.macros :refer [go]]))


;; :create-phone-number
(re-frame/reg-event-fx
  :create-phone-number
  (fn [{:keys [db]} [_ new-phone-number feedback-channel]]
    {:http-xhrio {:method :post
                  :uri "/api/v1/funnels/phone_numbers/"
                  :params {"number" (:number new-phone-number)
                           "greeting" (:greeting new-phone-number)}
                  :format (ajax/url-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :headers {"X-CSRFToken" (:csrf db)}
                  :on-success [:phone-number-created feedback-channel]
                  :on-failure [:ajax-call-fail feedback-channel]}}))


;; :phone-number-created
(re-frame/reg-event-db
  :phone-number-created
  (fn [db [_ feedback-channel new-phone-number]]
    (helpers/respond-to-channel feedback-channel [true new-phone-number])
    (-> db
        (assoc-in [:phone-numbers (helpers/int->keyword (:id new-phone-number))] new-phone-number)
        (dissoc db :support-phones))))


;; :load-support-phones
(re-frame/reg-event-fx
  :load-support-phones
  (fn [{:keys [db]} [_ area-code]]
    (if (= area-code "")
      {:db (dissoc db :support-phones)}
      {:db (-> db
               (assoc :support-phones-loading true)
               (dissoc :support-phones))
       :http-xhrio {:method :get
                    :uri (str "/api/v1/funnels/support_phones/" area-code "/")
                    :params nil
                    :format (ajax/url-request-format)
                    :response-format (ajax/json-response-format)
                    :headers {"X-CSRFToken" (:csrf db)}
                    :on-success [:support-phones-loaded]}})))

;; :support-phones-loaded
(re-frame/reg-event-db
  :support-phones-loaded
  (fn [db [_ support-phones]]
    (-> db
        (assoc :support-phones support-phones)
        (dissoc :support-phones-loading))))


;; :update-phone-number
(re-frame/reg-event-fx
  :update-phone-number
  (fn [{:keys [db]} [_ updated-phone-number feedback-channel]]
    {:http-xhrio {:method :patch
                  :uri (str "/api/v1/funnels/phone_numbers/" (:id updated-phone-number) "/")
                  :params {"greeting" (:greeting updated-phone-number)}
                  :format (ajax/url-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :headers {"X-CSRFToken" (:csrf db)}
                  :on-success [:phone-number-updated feedback-channel]
                  :on-failure [:ajax-call-fail feedback-channel]}}))

;; :phone-number-updated
(re-frame/reg-event-db
  :phone-number-updated
  (fn [db [_ feedback-channel updated-phone-number]]
    (helpers/respond-to-channel feedback-channel [true updated-phone-number])
    (-> db
        (assoc-in [:phone-numbers (helpers/int->keyword (:id updated-phone-number))] updated-phone-number))))
