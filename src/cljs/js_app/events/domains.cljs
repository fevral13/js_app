(ns js-app.events.domains
  (:require [js-app.helpers :as helpers]
            [re-frame.core :as re-frame]
            [ajax.core :as ajax]
            [cljs.core.async :refer [>! close!]])
  (:require-macros [cljs.core.async.macros :refer [go]]))


;; :create-domain
(re-frame/reg-event-fx
  :create-domain
  (fn [{:keys [db]} [_ data feedback-channel]]
    {:http-xhrio {:method :post
                  :uri (str "/api/v1/funnels/domains/")
                  :params (helpers/map-keys-to-string data)
                  :format (ajax/url-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :headers {"X-CSRFToken" (:csrf db)}
                  :on-success [:domain-created feedback-channel]
                  :on-failure [:domain-create-fail feedback-channel]}}))


;; :domain-created
(re-frame/reg-event-db
  :domain-created
  (fn [db [_ feedback-channel new-domain]]
    (helpers/respond-to-channel feedback-channel [true new-domain])
    (-> db
        (assoc-in [:domains (helpers/int->keyword (:id new-domain))] new-domain))))


;; :domain-create-fail
(re-frame/reg-event-fx
  :domain-create-fail
  (fn [_ [_ feedback-channel {:keys [response]}]]
    (helpers/respond-to-channel feedback-channel [false (:error response)])
    {}))
