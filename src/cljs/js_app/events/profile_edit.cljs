(ns js-app.events.profile-edit
  (:require [ajax.core :as ajax]
            [re-frame.core :as re-frame]
            [js-app.helpers :as helpers]))


;; :set-profile-data
(re-frame/reg-event-fx
  :update-user-profile
  (fn [{:keys [db]} [_ profile-data feedback-channel]]
    {:http-xhrio {:method :patch
                  :uri "/api/v1/user/"
                  :params (helpers/map-keys-to-string profile-data)
                  :format (ajax/url-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :headers {"X-CSRFToken" (:csrf db)}
                  :on-success [:user-profile-updated feedback-channel]
                  :on-failure [:ajax-call-fail feedback-channel]}}))


;; :user-profile-updated
(re-frame/reg-event-db
  :user-profile-updated
  (fn [db [_ feedback-channel user-profile]]
    (helpers/respond-to-channel feedback-channel [true user-profile])
    (assoc db :user-profile user-profile)))


;; :update-stripe-account
(re-frame/reg-event-fx
  :update-stripe-account
  (fn [{:keys [db]} [_ account]]
      {:http-xhrio {:method :patch
                    :uri (str "/api/v1/funnels/stripe_accounts/" (:id account) "/")
                    :params {"name" (:name account)
                             "api_key" (:api_key account)
                             "api_secret" (:api_secret account)}
                    :format (ajax/url-request-format)
                    :response-format (ajax/json-response-format {:keywords? true})
                    :headers {"X-CSRFToken" (:csrf db)}
                    :on-success [:stripe-account-updated]}}))

;; :stripe-account-updated
(re-frame/reg-event-db
  :stripe-account-updated
  (fn [db [_ account]]
    (assoc-in db [:stripe-accounts (helpers/int->keyword (:id account))] account)))

;; :add-stripe-account
(re-frame/reg-event-fx
  :add-stripe-account
  (fn [{:keys [db]} [_ new-stripe-account]]
    {:http-xhrio {:method :post
                  :uri "/api/v1/funnels/stripe_accounts/"
                  :params {"name" (:name new-stripe-account)
                           "api_key" (:api_key new-stripe-account)
                           "api_secret" (:api_secret new-stripe-account)}
                  :format (ajax/url-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :headers {"X-CSRFToken" (:csrf db)}
                  :on-success [:stripe-account-created]}}))

;; :stripe-account-created
(re-frame/reg-event-db
  :stripe-account-created
  (fn [db [_ stripe-account]]
    (assoc-in db [:stripe-accounts (helpers/int->keyword (:id stripe-account))] stripe-account)))

;; :delete-stripe-account
(re-frame/reg-event-fx
  :delete-stripe-account
  (fn [{:keys [db]} [_ account-id]]
     {:http-xhrio {:method :delete
                   :uri (str "/api/v1/funnels/stripe_accounts/" account-id "/")
                   :params {}
                   :format (ajax/url-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :headers {"X-CSRFToken" (:csrf db)}
                   :on-success [:stripe-account-deleted account-id]}}))

;; :stripe-account-deleted
(re-frame/reg-event-db
  :stripe-account-deleted
  (fn [db [_ account-id _]]
    (assoc db :stripe-accounts (dissoc (:stripe-accounts db) (helpers/int->keyword account-id)))))
