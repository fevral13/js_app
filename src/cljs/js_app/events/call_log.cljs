(ns js-app.events.call-log
  (:require [re-frame.core :as re-frame]))

;; :call-log-loaded
(re-frame/reg-event-db
  :call-log-loaded
  (fn [db [_ call-log]]
    (-> db
        (assoc :call-log call-log))))

;; :clean-call-log
(re-frame/reg-event-db
  :clean-call-log
  (fn [db [_]]
    (dissoc db :call-log)))





