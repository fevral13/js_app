(ns js-app.events.product-packages
  (:require [ajax.core :as ajax]
            [re-frame.core :as re-frame]
            [js-app.helpers :as helpers]))

;; :add-product-package
(re-frame/reg-event-fx
  :add-product-package
  (fn [{:keys [db]} [_ product-package feedback-channel]]
    {:http-xhrio {:method :post
                  :uri "/api/v1/funnels/product_packages/"
                  :params {"funnel" (:funnel product-package)
                           "user_product" (:user_product product-package)
                           "quantity" (:quantity product-package)
                           "price" (:price product-package)}
                  :format (ajax/url-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :headers {"X-CSRFToken" (:csrf db)}
                  :on-success [:product-package-created feedback-channel]
                  :on-failure [:ajax-call-fail feedback-channel]}}))


;; :product-package-created
(re-frame/reg-event-db
  :product-package-created
  (fn [db [_ feedback-channel new-product-package]]
    (helpers/respond-to-channel feedback-channel [true new-product-package])
    (assoc-in db [:product-packages (helpers/int->keyword (:id new-product-package))] new-product-package)))


;; :delete-product-package
(re-frame/reg-event-fx
  :delete-product-package
  (fn [{:keys [db]} [_ product-package-id]]
    {:http-xhrio {:method :delete
                  :uri (str "/api/v1/funnels/product_packages/" product-package-id "/")
                  :params {}
                  :format (ajax/url-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :headers {"X-CSRFToken" (:csrf db)}
                  :on-success [:product-package-deleted product-package-id]}}))


;; :product-package-deleted
(re-frame/reg-event-db
  :product-package-deleted
  (fn [db [_ product-package-id _]]
    (let [existing-packages (:product-packages db)]
      (assoc db :product-packages (dissoc existing-packages (helpers/int->keyword product-package-id))))))

