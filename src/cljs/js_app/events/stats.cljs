(ns js-app.events.stats
  (:require [re-frame.core :as re-frame]
            [js-app.helpers :as helpers]))


;; :visitor-stats-loaded
(re-frame/reg-event-db
  :visitor-stats-loaded
  (fn [db [_ funnel-id visitor-stats]]
    (assoc-in db [:visitor-stats (helpers/int->keyword funnel-id)] visitor-stats)))


;; :clean-visitor-stats
(re-frame/reg-event-db
  :clean-visitor-stats
  (fn [db [_ funnel-id]]
    (update-in db [:visitor-stats] dissoc (helpers/int->keyword funnel-id))))


;; :aggregated-stats-loaded
(re-frame/reg-event-db
  :aggregated-stats-loaded
  (fn [db [_ funnel-id aggregated-stats]]
    (assoc-in db [:aggregated-stats (helpers/int->keyword funnel-id)] aggregated-stats)))


;; :clean-aggregated-stats
(re-frame/reg-event-db
  :clean-aggregated-stats
  (fn [db [_ funnel-id]]
    (update-in db [:aggregated-stats] dissoc (helpers/int->keyword funnel-id))))
