(ns js-app.events.upsell-packages
  (:require [ajax.core :as ajax]
            [re-frame.core :as re-frame]
            [js-app.helpers :as helpers]))

;; :add-upsell-package
(re-frame/reg-event-fx
  :add-upsell-package
  (fn [{:keys [db]} [_ upsell-package feedback-channel]]
    {:http-xhrio {:method :post
                  :uri "/api/v1/funnels/upsell_packages/"
                  :params (helpers/map-keys-to-string upsell-package)
                  :format (ajax/url-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :headers {"X-CSRFToken" (:csrf db)}
                  :on-success [:upsell-package-created feedback-channel]
                  :on-failure [:ajax-call-fail feedback-channel]}}))


;; :upsell-package-created
(re-frame/reg-event-db
  :upsell-package-created
  (fn [db [_ feedback-channel new-upsell-package]]
    (helpers/respond-to-channel feedback-channel [true new-upsell-package])
    (assoc-in db [:upsell-packages (helpers/int->keyword (:id new-upsell-package))] new-upsell-package)))


;; :update-upsell-package
(re-frame/reg-event-fx
  :update-upsell-package
  (fn [{:keys [db]} [_ upsell-package]]
    {:http-xhrio {:method :patch
                  :uri (str "/api/v1/funnels/upsell_packages/" (:id upsell-package) "/")
                  :params (helpers/map-keys-to-string upsell-package)
                  :format (ajax/url-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :headers {"X-CSRFToken" (:csrf db)}
                  :on-success [:upsell-package-updated]}}))


;; :upsell-package-updated
(re-frame/reg-event-db
  :upsell-package-updated
  (fn [db [_ updated-upsell-package]]
    (assoc-in db [:upsell-packages (helpers/int->keyword (:id updated-upsell-package))] updated-upsell-package)))


;; :delete-upsell-package
(re-frame/reg-event-fx
  :delete-upsell-package
  (fn [{:keys [db]} [_ upsell-package-id]]
    {:http-xhrio {:method :delete
                  :uri (str "/api/v1/funnels/upsell_packages/" upsell-package-id "/")
                  :params {}
                  :format (ajax/url-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :headers {"X-CSRFToken" (:csrf db)}
                  :on-success [:upsell-package-deleted upsell-package-id]}}))


;; :upsell-package-deleted
(re-frame/reg-event-db
  :upsell-package-deleted
  (fn [db [_ upsell-package-id _]]
    (let [existing-packages (:upsell-packages db)]
      (assoc db :upsell-packages (dissoc existing-packages (helpers/int->keyword upsell-package-id))))))

