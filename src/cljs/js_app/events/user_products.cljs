(ns js-app.events.user-products
  (:require [re-frame.core :as re-frame]
            [js-app.helpers :as helpers]
            [ajax.core :as ajax]))


;; :add-user-product
(re-frame/reg-event-fx
  :add-user-product
  (fn [{:keys [db]} [_ product feedback-channel]]
    {:http-xhrio {:method :post
                  :uri "/api/v1/funnels/user_products/"
                  :params {"product" (:id product)
                           "name" (:name product)
                           "description" (:description product)
                           "price" (:price product)}
                  :format (ajax/url-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :headers {"X-CSRFToken" (:csrf db)}
                  :on-success [:user-product-created feedback-channel]}}))


;; :user-product-created
(re-frame/reg-event-db
  :user-product-created
  (fn [db [_ feedback-channel new-user-product]]
    (helpers/respond-to-channel feedback-channel [true new-user-product])
    (assoc-in db [:user-products (:id new-user-product)] new-user-product)))


;; :update-user-product
(re-frame/reg-event-fx
  :update-user-product
  (fn [{:keys [db]} [_ user-product feedback-channel]]
    {:http-xhrio {:method :patch
                  :uri (str "/api/v1/funnels/user_products/" (:id user-product) "/")
                  :params {"name" (:name user-product)
                           "description" (:description user-product)
                           "phone_number" (:phone_number user-product)
                           "price" (:price user-product)}
                  :format (ajax/url-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :headers {"X-CSRFToken" (:csrf db)}
                  :on-success [:user-product-updated feedback-channel]
                  :on-failure [:ajax-call-fail feedback-channel]}}))


;; :user-product-updated
(re-frame/reg-event-db
  :user-product-updated
  (fn [db [_ feedback-channel user-product]]
    (helpers/respond-to-channel feedback-channel [true user-product])
    (assoc-in db [:user-products (helpers/int->keyword (:id user-product))] user-product)))
