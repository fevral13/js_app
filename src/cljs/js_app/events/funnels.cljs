(ns js-app.events.funnels
  (:require [js-app.helpers :as helpers]
            [re-frame.core :as re-frame]
            [ajax.core :as ajax]))


;; :create-funnel
(re-frame/reg-event-fx
  :create-funnel
  (fn [{:keys [db]} [_ new-funnel feedback-channel]]
    (let [request-data (helpers/map-keys-to-string new-funnel)]
      {:http-xhrio {:method :post
                    :uri "/api/v1/funnels/funnels/"
                    :params request-data
                    :format (ajax/url-request-format)
                    :response-format (ajax/json-response-format {:keywords? true})
                    :headers {"X-CSRFToken" (:csrf db)}
                    :on-success [:funnel-created feedback-channel]
                    :on-failure [:ajax-call-fail feedback-channel]}})))

;; :funnel-created
(re-frame/reg-event-db
  :funnel-created
  (fn [db [_ feedback-channel new-funnel]]
    (helpers/respond-to-channel feedback-channel [true new-funnel])
    (-> db
      (assoc-in [:funnels (helpers/int->keyword (:id new-funnel))] new-funnel)
      (dissoc db :support-phones))))

;; :update-funnel
(re-frame/reg-event-fx
  :update-funnel
  (fn [{:keys [db]} [_ data feedback-channel]]
    (let [request-data (helpers/map-keys-to-string data)]
      {:http-xhrio {:method :patch
                    :uri (str "/api/v1/funnels/funnels/" (:id data) "/")
                    :params request-data
                    :format (ajax/url-request-format)
                    :response-format (ajax/json-response-format {:keywords? true})
                    :headers {"X-CSRFToken" (:csrf db)}
                    :on-success [:funnel-updated feedback-channel]
                    :on-failure [:ajax-call-fail feedback-channel]}})))

;; :funnel-updated
(re-frame/reg-event-db
  :funnel-updated
  (fn [db [_ feedback-channel updated-funnel]]
    (helpers/respond-to-channel feedback-channel [true updated-funnel])
    (assoc-in db [:funnels (helpers/int->keyword (:id updated-funnel))] updated-funnel)))


;; :change-primary-product
(re-frame/reg-event-fx
  :change-primary-product
  (fn [{:keys [db]} [_ data feedback-channel]]
    (let [request-data (helpers/map-keys-to-string data)
          funnel-id (:id data)
          new-primary-product (:primary_product data)
          old-primary-product (get-in db [:funnels (helpers/int->keyword funnel-id) :primary_product])]
      (if (not= new-primary-product old-primary-product)
        ;; only if product changed
        {:http-xhrio {:method :patch
                      :uri (str "/api/v1/funnels/funnels/" (:id data) "/")
                      :params request-data
                      :format (ajax/url-request-format)
                      :response-format (ajax/json-response-format {:keywords? true})
                      :headers {"X-CSRFToken" (:csrf db)}
                      :on-success [:primary-product-changed feedback-channel]}}
        ;; otherwise just ping pack feedback channel and return empty effect map
        (do
          (helpers/respond-to-channel feedback-channel [true data])
          {})))))


;; :funnel-updated
(re-frame/reg-event-fx
  :primary-product-changed
  (fn [{:keys [db]} [_ feedback-channel updated-funnel]]
    (let [funnel-id (:id updated-funnel)
          product-packages-to-delete (filter #(= funnel-id (:funnel %))
                                             (vals (:product-packages db)))]
      (helpers/respond-to-channel feedback-channel [true updated-funnel])
      ;; update primary product
      {:db (assoc-in db [:funnels (helpers/int->keyword (:id updated-funnel))] updated-funnel)
       ;; delete all previuous product packages
       :dispatch-n (into [] (map (fn [{id :id}] [:delete-product-package id])
                                 product-packages-to-delete))})))
