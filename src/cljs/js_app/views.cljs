(ns js-app.views
  (:require [re-frame.core :as re-frame]
            [free-form.re-frame :as ff]
            [free-form.core :as free-form]
            [js-app.helpers :as helpers]
            [js-app.routes :refer [resolve-path navigate]]
            [js-app.validators :as validators]
            [js-app.constants :as constants]
            free-form.bootstrap-3
            [reagent.core :as reagent]
            [reagent-modals.modals :as modals]
            [cljs.core.async :as async :refer [<! chan]])
  (:require-macros [cljs.core.async.macros :refer [go]]))


;; generic components
;; ==============================

(defn ajax-loader []
  [:img {:src "/static/images/ajax-loader.gif"}])

(defn ajax-loader-or-text [ajax-active text]
  (if @ajax-active
    [ajax-loader]
    text))

;; navpanel
;; ==============================

(defn navbar []
  (let [active-panel (re-frame/subscribe [:active-panel])
        user-profile (re-frame/subscribe [:user-profile])
        global-issues (re-frame/subscribe [:global-issues])]
    (fn []
      (let [global-issues-count (count @global-issues)
            global-issues-exist (> global-issues-count 0)]
        [:nav.navbar.navbar-default.navbar-fixed-top
         [:div.container
          [:div.navbar-header
           [:button.navbar-toggle.collapsed {:type "button"
                                             :data-toggle "collapse"
                                             :data-target "#navbar"
                                             :aria-expanded "false"
                                             :aria-controls "navbar"}
            [:span.sr-only "Toggle navigation"]
            [:span.icon-bar]
            [:span.icon-bar]
            [:span.icon-bar]]]
          [:a.navbar-brand {:href (resolve-path :home-panel)} "Profitlaunchhero"]
          [:div#navbar.collapse.navbar-collapse
           [:ul.navbar-nav.nav
            [:li.nav-item {:className (when (= @active-panel :home-panel) "active")}
             [:a.nav-link {:href (resolve-path :home-panel)} "Home"]]

            ;[:li.nav-item {:className (when (= @active-panel :dashboard-panel) "active")}
            ; [:a.nav-link {:href (resolve-path :dashboard-panel)} "Dashboard"]]
            ;
            [:li.nav-item {:className (when (contains? #{:user-products-panel :select-products-panel} @active-panel) "active")}
             [:a.nav-link {:href (resolve-path :user-products-panel)} "Products"]]

            [:li.nav-item {:className (when (contains? #{:funnels-panel :create-funnel-panel} @active-panel) "active")}
             [:a.nav-link {:href (resolve-path :funnels-panel)} "Funnels"]]

            [:li.nav-item {:className (when (contains? #{:domains-panel :new-domain-panel} @active-panel) "active")}
             [:a.nav-link {:href (resolve-path :domains-panel)} "Domains"]]

            [:li.nav-item {:className (when (contains? #{:phones-panel :new-phone-number-panel} @active-panel) "active")}
             [:a.nav-link {:href (resolve-path :phones-panel)} "Phones"]]

            [:li.dropdown {:className (when (contains? #{:reports-call-log-panel :reports-visitor-stats-panel} @active-panel) "active")}
             [:a.dropdown-toggle {:data-toggle "dropdown"
                                  :role "button"} "Reports" [:span.caret]]
             [:ul.dropdown-menu
              [:li {:className (when (= @active-panel :reports-call-log-panel) "active")}
               [:a.nav-link {:href (resolve-path :reports-call-log-panel)} "Call log"]]
              [:li {:className (when (= @active-panel :reports-visitor-stats-panel) "active")}
               [:a.nav-link {:href (resolve-path :reports-visitor-stats-panel)} "Visitor stats"]]]]

            (when global-issues-exist
              [:li.dropdown.issue-nav-item
               [:a.dropdown-toggle {:data-toggle "dropdown"
                                    :role "button"} (str "Issues (" global-issues-count ")")
                [:span.caret]]
               [:ul.dropdown-menu.issue-nav-item
                (for [[_ message url] @global-issues]
                  [:li {:key message}
                   [:a.nav-link {:href url} message]])]])]

           [:ul.navbar-nav.nav.navbar-right
            [:li.dropdown {:className (when (contains? #{:profile-panel :stripe-accounts-panel} @active-panel) "active")}
             [:a.dropdown-toggle {:data-toggle "dropdown"
                                  :role "button"} (:email @user-profile) [:span.caret]]
             [:ul.dropdown-menu
              [:li.nav-item {:className (when (= @active-panel :profile-panel) "active")}
               [:a.nav-link {:href (resolve-path :profile-panel)} "Profile"]]
              [:li {:className (when (= @active-panel :stripe-accounts-panel) "active")}
               [:a.nav-link {:href (resolve-path :stripe-accounts-panel)} "Stripe accounts"]]]]

            [:li.nav-item
             [:a.nav-link {:href "/accounts/logout"} "Logout"]]]]]]))))


;; home
;; ==============================

(defn home-panel []
  [:div "HOME"])


;; dashboard panel
;; ==============================

(defn dashboard-panel []
  (fn []
    [:div "DASHBOARD"]))


;; new phone panel
;; ==============================

(defn new-phone-number-panel []
  (let [form-state (reagent/atom {:greeting (deref (re-frame/subscribe [:default-greeting]))})
        form-errors (reagent/atom {})
        support-phone-area-codes (re-frame/subscribe [:support-phone-area-codes])
        support-phones (re-frame/subscribe [:support-phones])
        support-phones-loading (re-frame/subscribe [:support-phones-loading])
        ajax-active (reagent/atom false)]
    (fn []
      [:div
       [:div.row
        [:div.col-md-6.col-md-offset-3
         [:h1 "Get new phone number"]

         [free-form/form
          @form-state
          @form-errors
          (helpers/form-update-handler form-state)
          :bootstrap-3
          [:form
           [:free-form/field {:label "Greeting"
                              :type :textarea
                              :key :greeting}]
           [:div.form-group
            [:div.row
             [:div.col-md-4
              [:label "Area code"]
              [:select.form-control
               {:on-change (fn [e]
                             (let [area-code (.-value (.-target e))]
                               (re-frame/dispatch [:load-support-phones area-code])))}

               [:option {:value ""} "Area code"]
               (for [[phone-prefix area-name] @support-phone-area-codes]
                 [:option
                  {:value phone-prefix
                   :key phone-prefix}
                  (str phone-prefix " " area-name)])]]

             [:div.col-md-8
              [:div.form-group {:free-form/error-class {:key :number :error "has-error"}}
               [:label.control-label "Support phone"]
               [:br]
               (if @support-phones-loading
                 [ajax-loader]

                 (if @support-phones
                   ((fn [phones]
                      [:div
                       [:select.form-control {:free-form/input {:key :number}}
                        [:option {:value ""} "Select phone number"]
                        (for [phone phones]
                          [:option {:value phone :key phone} phone])]
                       [:div.text-danger {:free-form/error-message {:key :number}} [:p.error]]]) @support-phones)
                   [:div "Select area code"]))]]]]
           [:div.form-group
            [:a.btn.btn-success {:role :button
                                 :on-click (helpers/form-submit-handler
                                             validators/new-phone-number-validator
                                             form-state
                                             form-errors
                                             ajax-active
                                             :create-phone-number
                                             (fn [] (navigate :phones-panel)))}
             (ajax-loader-or-text ajax-active "Get")]
            [:a.btn {:href (resolve-path :phones-panel)} "Cancel"]]]]]]])))

;; phones panel
;; ==============================

(defn phone-greeting-view-panel [phone-number greeting-show-mode]
  [:div
   [:a.btn.btn-default.float-right {:role :button
                                    :on-click (fn [_] (reset! greeting-show-mode :edit))}
    "Edit"]
   [:div.phone-greeting-wrapper
    (:greeting phone-number)]])


(defn phone-greeting-edit-panel [phone-number greeting-show-mode]
  (let [form-state (reagent/atom (select-keys phone-number [:id :greeting]))
        form-errors (reagent/atom {})
        ajax-active (reagent/atom false)]
    (fn [phone-number greeting-show-mode]
      [free-form/form @form-state @form-errors (helpers/form-update-handler form-state) :bootstrap-3
       [:form
        [:free-form/field {:type :textarea
                           :key :greeting}]
        [:div.form-group
         [:a.btn.btn-success
          {:role :button
           :disabled @ajax-active
           :on-click (helpers/form-submit-handler
                       validators/phone-number-validator
                       form-state
                       form-errors
                       ajax-active
                       :update-phone-number
                       (fn [] (reset! greeting-show-mode :show)))}
          (ajax-loader-or-text ajax-active "Save")]
         [:a.btn
          {:role :button
           :on-click (fn [] (reset! greeting-show-mode :show))}
          "Cancel"]]]])))


(defn phone-row [phone-number]
  (let [greeting-show-mode (reagent/atom :show)]
    (fn [phone-number]
      [:tr {:key (:id phone-number)}
       [:td
        (:number phone-number)]
       [:td
        (for [product (:user-products phone-number)]
          [:p {:key (:id product)} (:name product)])]
       [:td
        (case @greeting-show-mode
          :show [phone-greeting-view-panel phone-number greeting-show-mode]
          :edit [phone-greeting-edit-panel phone-number greeting-show-mode])]])))


(defn phones-panel []
  (let [phone-numbers (re-frame/subscribe [:phone-numbers-annotated])]
    (fn []
      [:div
       [:h1 "Phones"
        [:a.btn.btn-success.float-right {:href (resolve-path :new-phone-number-panel)} "New phone number"]]

       [:table.table.table-bordered
        [:thead
         [:tr
          [:th "Phone number"]
          [:th "Products"]
          [:th "Greeting"]]]
        [:tbody
         (for [phone-number @phone-numbers]
           [phone-row phone-number])]]])))

;; domains panel
;; ==============================

(defn new-domain-panel []
  (let [form-state (reagent/atom {})
        form-errors (reagent/atom {})
        ajax-active (reagent/atom false)]
    (fn []
       [:div.row
        [:div.col-md-6.col-md-offset-3
         [:h1 "Get new domain"]

         [free-form/form
          @form-state
          @form-errors
          (helpers/form-update-handler form-state)
          :bootstrap-3
          [:form
           [:free-form/field {:label "Domain name"
                              :type :text
                              :key :domain}]

           [:div.form-group
            [:button.btn.btn-success {:role :button
                                      :on-click (helpers/form-submit-handler
                                                  validators/new-domain-name-validator
                                                  form-state
                                                  form-errors
                                                  ajax-active
                                                  :create-domain
                                                  (fn []
                                                    (navigate :domains-panel)))}
             (ajax-loader-or-text ajax-active "Get")]

            [:a.btn {:href (resolve-path :domains-panel)} "Cancel"]]]]]])))


(defn domains-panel []
  (let [domains (re-frame/subscribe [:domains])]
    (fn []
      [:div
       [:h1 "Your domains"
        [:a.btn.btn-success.float-right {:href (resolve-path :new-domain-panel)} "New domain"]]
       (for [domain @domains]
         (let [funnels (:funnels domain)
               funnels-exist (> (count funnels) 0)]
           [:div.row.domain-row {:key (:id domain)}
            [:div.col-md-6
             [:h3 (:domain domain)]]
            [:div.col-md-6
             (if funnels-exist
               [:table.table.table-bordered
                [:thead
                 [:tr
                  [:th "Funnel"]
                  [:th "Path"]]]
                [:tbody
                 (for [funnel funnels]
                   [:tr {:key (:id funnel)}
                    [:td (:name funnel)]
                    [:td
                     [:a {:href (str "//" (:domain domain) (:path funnel))
                          :target :_blank}
                      (:path funnel)]]])]]
               [:span "No funnel is using this domain"])]]))])))


;; create funnel panel
;; ==========================

(defn create-funnel-panel []
  (let [form-state (reagent/atom (helpers/init-map [:name :domain :path :primary_product :stripe_account]))
        form-errors (reagent/atom {})
        ajax-active (reagent/atom false)
        user-products (re-frame/subscribe [:user-products])
        domains (re-frame/subscribe [:domains])
        stripe-accounts (re-frame/subscribe [:stripe-accounts])
        funnel-templates (re-frame/subscribe [:funnel-templates])

        domain-select-options (concat ["" "Select one"]
                                      (mapcat (fn [domain] [(:id domain) (:domain domain)])
                                              @domains))
        user-product-select-options (concat ["" "Select one"]
                                            (mapcat (fn [user-product] [(:id user-product) (:name user-product)])
                                                    (vals @user-products)))
        stripe-accounts-select-options (concat ["" "Select one"]
                                               (mapcat (fn [account] [(:id account) (:name account)])
                                                       (vals @stripe-accounts)))
        funnel-templates-select-options (concat ["" "Select one"]
                                                (mapcat (fn [template] [(:id template) (:title template)])
                                                        (vals @funnel-templates)))]

    (fn []
      [:div
       [:div.row
        [:div.col-md-6.col-md-offset-3
         [:h1 "Create funnel"]
         [free-form/form
          @form-state
          @form-errors
          (helpers/form-update-handler form-state)
          :bootstrap-3
          [:form
           [:free-form/field {:type :text
                              :key :name
                              :label "Funnel name"}]

           [:free-form/field {:type :select
                              :key :template
                              :label "Template"
                              :options funnel-templates-select-options}]

           [:free-form/field {:type :select
                              :key :domain
                              :label "Domain"
                              :options domain-select-options}]

           [:free-form/field {:type :text
                              :key :path
                              :label "Path"}]

           [:free-form/field {:type :select
                              :key :primary_product
                              :label "Primary product"
                              :options user-product-select-options}]

           [:free-form/field {:type :select
                              :key :stripe_account
                              :label "Stripe accounts"
                              :options stripe-accounts-select-options}]

           [:div.form-group
            [:button.btn.btn-primary {:role :button
                                      :on-click (helpers/form-submit-handler
                                                  validators/new-funnel-validator
                                                  form-state
                                                  form-errors
                                                  ajax-active
                                                  :create-funnel
                                                  (fn [] (navigate :funnels-panel)))}
             (ajax-loader-or-text ajax-active "Create")]
            [:a.btn {:role :button
                     :on-click (fn [] (navigate :funnels-panel))}
             "Cancel"]]]]]]])))


;; funnels panel
;; ==============================

(defn set-active-tab [current-tab-atom tab-name]
  (fn [e]
    (do
      (.preventDefault e)
      (reset! current-tab-atom tab-name))))


(defn package-item [package]
  [:div.package-row {:key (:id package)}
   [:span (str "Package " (:quantity package) " items for $" (:price package))]
   [:a.delete-package-button
    {:role :button
     :on-click (fn [e]
                 (.preventDefault e)
                 (re-frame/dispatch [:delete-product-package (:id package)]))}
    "Delete"]])


(defn create-package-form [funnel]
  (let [package-template {:funnel (:id funnel)
                          :user_product (:primary_product funnel)}
        form-state (reagent/atom package-template)
        form-errors (reagent/atom {})
        ajax-active (reagent/atom false)]
    (fn [funnel]
      [free-form/form @form-state @form-errors (helpers/form-update-handler form-state) :bootstrap-3
       [:form.padded
        [:free-form/field {:type :text
                           :key :quantity
                           :label "Quantity"}]
        [:free-form/field {:type :text
                           :key :price
                           :label "Price"}]
        [:div.form-group
         [:button.btn.btn-success
          {:role :button
           :disabled @ajax-active
           :on-click (helpers/form-submit-handler
                       validators/package-validator
                       form-state
                       form-errors
                       ajax-active
                       :add-product-package
                       (fn [] (reset! form-state package-template)))}

          (ajax-loader-or-text ajax-active "Add package")]]]])))


(defn packages-panel [funnel]
  (let [product-packages (re-frame/subscribe [:product-packages (:id funnel) (:primary_product funnel)])]
    (fn [funnel]
      [:div.row
       [:div.col-md-8
        (for [package @product-packages]
          [package-item package])]
       [:div.col-md-4
        [create-package-form funnel]]])))


(defn create-upsell-package-form [funnel]
  (let [package-template {:funnel (:id funnel)}
        user-products (re-frame/subscribe [:user-products])
        user-product-select-options (concat ["" "Select one"]
                                            (mapcat (fn [user-product] [(:id user-product) (:name user-product)])
                                                    (vals @user-products)))
        form-state (reagent/atom package-template)
        form-errors (reagent/atom {})
        ajax-active (reagent/atom false)]
    (fn [funnel]
      [free-form/form @form-state @form-errors (helpers/form-update-handler form-state) :bootstrap-3
       [:form.padded
        [:free-form/field {:type :select
                           :key :user_product
                           :label "Product"
                           :options user-product-select-options}]
        [:free-form/field {:type :text
                           :key :quantity
                           :label "Quantity"}]
        [:free-form/field {:type :text
                           :key :price
                           :label "Price"}]
        [:div.form-group
         [:button.btn.btn-success
          {:role :button
           :disabled @ajax-active
           :on-click (helpers/form-submit-handler
                       validators/upsell-package-validator
                       form-state
                       form-errors
                       ajax-active
                       :add-upsell-package
                       (fn [] (reset! form-state package-template)))}

          (ajax-loader-or-text ajax-active "Add upsell")]]]])))


(defn upsells-panel [funnel]
  (let [upsell-packages (re-frame/subscribe [:upsell-packages (:id funnel)])]
    (fn [funnel]
      (let [indexed-upsell-packages (map-indexed vector @upsell-packages)
            upsell-packages-map (into {} indexed-upsell-packages)
            upsell-package-number (count indexed-upsell-packages)]
        [:div.row
         [:div.col-md-8
          (doall
            (for [[index upsell-package] indexed-upsell-packages]
              (let [product (re-frame/subscribe [:user-product (:user_product upsell-package)])]
                [:div.row.upsell-package-row {:key (:id upsell-package)}
                 [:div.col-md-2
                  (when (not= index 0)
                    [:a {:role :button
                         :on-click (fn [e]
                                     (let [prev-package (get upsell-packages-map (- index 1))]
                                       (.preventDefault e)
                                       (re-frame/dispatch [:update-upsell-package (assoc upsell-package :order (:order prev-package))])
                                       (re-frame/dispatch [:update-upsell-package (assoc prev-package :order (:order upsell-package))])))}
                     "Up"])
                  [:br]
                  (when (not= index (- upsell-package-number 1))
                    [:a {:role :button
                         :on-click (fn [e]
                                     (let [next-package (get upsell-packages-map (+ index 1))]
                                       (.preventDefault e)
                                       (re-frame/dispatch [:update-upsell-package (assoc upsell-package :order (:order next-package))])
                                       (re-frame/dispatch [:update-upsell-package (assoc next-package :order (:order upsell-package))])))}
                     "Down"])]
                 [:div.col-md-2
                  [:img.upsell-product-image {:src (:image @product)}]]
                 [:div.col-md-6
                  [:span.upsell-product-name (:name @product)]
                  [:br]
                  [:span.upsell-quantity (:quantity upsell-package)]
                  " for "
                  [:span.upsell-price "$" (:price upsell-package)]]
                 [:div.col-md-2
                  [:a {:role :button
                       :on-click (fn [e]
                                   (.preventDefault e)
                                   (re-frame/dispatch [:delete-upsell-package (:id upsell-package)]))}
                   "Delete"]]])))]
         [:div.col-md-4
          [create-upsell-package-form funnel]]]))))


(defn path-panel [funnel]
  (let [form-state (reagent/atom (select-keys funnel [:id :domain :path]))
        form-errors (reagent/atom {})
        ajax-active (reagent/atom false)
        domains (re-frame/subscribe [:domains])
        domain-select-options (concat ["" "Select one"]
                                      (mapcat (fn [domain] [(:id domain) (:domain domain)])
                                              @domains))
        just-saved (reagent/atom false)]

    (fn [funnel]
      [:div
       [free-form/form
        @form-state
        @form-errors
        (helpers/form-update-handler form-state)
        :bootstrap-3
        [:form.padded
         [:free-form/field {:type :select
                            :key :domain
                            :label "Domain"
                            :options domain-select-options}]
         [:free-form/field {:key :path
                            :type :text
                            :label "Path"}]
         [:button.btn.btn-success
          {:on-click (helpers/form-submit-handler
                       validators/funnel-path-validator
                       form-state
                       form-errors
                       ajax-active
                       :update-funnel
                       (fn []
                         (reset! ajax-active false)
                         (reset! just-saved true)
                         (js/setTimeout (fn [] (reset! just-saved false)) constants/default-alert-timeout)))}
          (ajax-loader-or-text
            ajax-active
            "Save")]
         (when @just-saved [:a.btn "Saved!"])]]])))


(defn settings-panel [funnel]
  (let [form-state (reagent/atom (select-keys funnel [:id :stripe_account :name]))
        form-errors (reagent/atom {})
        ajax-active (reagent/atom false)
        stripe-accounts (re-frame/subscribe [:stripe-accounts])
        account-select-options (concat ["" "Select one"]
                                       (mapcat (fn [account] [(:id account) (:name account)])
                                               (vals @stripe-accounts)))
        just-saved (reagent/atom false)]

    (fn [funnel]
      [free-form/form
       @form-state
       @form-errors
       (helpers/form-update-handler form-state)
       :bootstrap-3
       [:form.padded
        [:free-form/field {:type :text
                           :key :name
                           :label "Name"}]
        [:free-form/field {:type :select
                           :key :stripe_account
                           :label "Stripe account"
                           :options account-select-options}]
        [:button.btn.btn-success
         {:on-click (helpers/form-submit-handler
                      validators/funnel-stripe-account-validator
                      form-state
                      form-errors
                      ajax-active
                      :update-funnel
                      (fn []
                        (reset! ajax-active false)
                        (reset! just-saved true)
                        (js/setTimeout (fn [] (reset! just-saved false)) constants/default-alert-timeout)))}
         (ajax-loader-or-text
           ajax-active
           "Save")]
        (when @just-saved [:a.btn "Saved!"])]])))


(defn get-panel [current-tab-atom]
  (case @current-tab-atom
    :packages packages-panel
    :upsells upsells-panel
    :path path-panel
    :settings settings-panel
    packages-panel))


(defn primary-product-show [primary-product primary-product-show-mode]
  (fn [primary-product primary-product-show-mode]
    [:div
     [:h2 (:name primary-product)]
     [:img.primary-product-image {:src (:image primary-product)}]
     [:h3 (str "Price: $" (:price primary-product))]
     [:a
      {:role :button
       :on-click (fn [] (reset! primary-product-show-mode :change))}
      "Change primary product"]]))


(defn primary-product-change [primary-product funnel primary-product-show-mode]
  (let [form-state (reagent/atom (select-keys funnel [:id :primary_product]))
        form-errors (reagent/atom {})
        ajax-active (reagent/atom false)
        user-products (re-frame/subscribe [:user-products])
        close-edit-form (fn [_] (reset! primary-product-show-mode :show))]
    (fn [primary-product funnel primary-product-show-mode]
      [:div
       [free-form/form
        @form-state
        @form-errors
        (helpers/form-update-handler form-state)
        :bootstrap-3
        [:form
         [:div.form-group
          [:label "Primary product"]
          [:select.form-control {:free-form/input {:key :primary_product}}
           (for [user-product (vals @user-products)]
             [:option {:key (:id user-product) :value (:id user-product)} (:name user-product)])]]
         (when (not= (str (:primary_product funnel)) (str (:primary_product @form-state)))
           [:div.alert.alert-warning "Changing primary product will delete all packages in this funnel!"])

         [:a.btn.btn-primary {:role :button
                              :on-click (helpers/form-submit-handler
                                          validators/primary-product-validator
                                          form-state
                                          form-errors
                                          ajax-active
                                          :change-primary-product
                                          close-edit-form)}
          (ajax-loader-or-text ajax-active "Change")]
         [:a.btn {:role :button
                  :on-click close-edit-form}
          "Cancel"]]]])))


(defn funnel-row [funnel]
  (let [current-tab (reagent/atom :packages)
        primary-product-show-mode (reagent/atom :show)
        primary-product (re-frame/subscribe [:primary-product (:id funnel)])
        template (re-frame/subscribe [:funnel-template (:template funnel)])]
    (fn [funnel]
      [:div.row.funnel-row {:key (:id funnel)}
       [:div.col-md-3
        [:h2 (:name funnel)]
        [:h3 (str "Template " (:title @template))]
        [:a {:role :button} "Open site editor"]]
       [:div.col-md-3
        (case @primary-product-show-mode
          :show [primary-product-show @primary-product primary-product-show-mode]
          :change [primary-product-change @primary-product funnel primary-product-show-mode])]
       [:div.col-md-6
        [:ul.nav.nav-tabs
         [:li {:role "presentation" :class (if (= @current-tab :packages) "active" "")}
          [:a {:role :button
               :on-click (set-active-tab current-tab :packages)} "Packages"]]

         [:li {:role "presentation" :class (if (= @current-tab :upsells) "active" "")}
          [:a {:role :button
               :on-click (set-active-tab current-tab :upsells)} "Upsells"]]

         [:li {:role "presentation" :class (if (= @current-tab :path) "active" "")}
          [:a {:role :button
               :on-click (set-active-tab current-tab :path)} "Path"]]

         [:li {:role "presentation" :class (if (= @current-tab :settings) "active" "")}
          [:a {:role :button
               :on-click (set-active-tab current-tab :settings)} "Settings"]]]
        [:div.panel.panel-default
         [:panel-body [(get-panel current-tab) funnel]]]]])))


(defn funnels-panel []
  (let [funnels (re-frame/subscribe [:funnels])]
    (fn []
      [:div
       [:div.row
        [:div.col-md-12
         [:h1 "Your funnels"
          [:a.btn.btn-primary.float-right {:href (resolve-path :create-funnel-panel)} "Add funnel"]]]]
       (for [funnel (vals @funnels)]
         [funnel-row funnel])])))


;; call log reports panel
;; ==============================

(defn reports-call-log-panel []
  (let [call-log (re-frame/subscribe [:call-log])]
    (fn []
      [:div
       [:h1 "Call Log"]
       [:table.table.table-bordered
        [:thead
         [:tr
          [:th "Date"]
          [:th "From"]
          [:th "To"]
          [:th "Duration"]
          [:th "Record"]]]
        [:tbody
         (if @call-log
           (for [call-item @call-log]
             [:tr {:key (:key call-item)}
              [:td (:date call-item)]
              [:td (:from call-item)]
              [:td (:to call-item)]
              [:td (:duration call-item)]
              [:td
               (when (:record call-item)
                 [:audio {:controls 1}
                  [:source {:src (:record call-item)}]])]])
           [:tr
            [:td.text-center {:col-span "4"} [ajax-loader]]])]]])))


;; visitor stats reports panel
;; ==============================

(defn sessions-chart-component [conf]
  (let [chart-atom (reagent/atom nil)
        update (fn [component]
                 (let [props (reagent/props component)
                       data (:data props)
                       labels (map :_id data)
                       total-sessions-data (map :t data)
                       total-visitors-data (map :u data)
                       new-visitors-data (map :n data)

                       options {"title" {"text" ""}
                                "tooltip" {}
                                "legend" {"show" true
                                          "data" [{"name" "Total Sessions"
                                                   "icon" "rect"
                                                   "textStyle" {"color" "red"}}
                                                  {"name" "Total Visitors"
                                                   "icon" "rect"
                                                   "textStyle" {"color" "green"}}
                                                  {"name" "New Visitors"
                                                   "icon" "rect"
                                                   "textStyle" {"color" "blue"}}]}
                                "xAxis" {"boundaryGap" true
                                         "data" labels}
                                "yAxis" {"type" "value"}
                                "series" [{"name" "Total Sessions"
                                           "type" "line"
                                           "smooth" true
                                           "data" total-sessions-data}
                                          {"name" "Total Visitors"
                                           "type" "line"
                                           "smooth" true
                                           "data" total-visitors-data}
                                          {"name" "New Visitors"
                                           "type" "line"
                                           "smooth" true
                                           "data" new-visitors-data}]}

                       js-chart-options (clj->js options)]
                   (.setOption @chart-atom js-chart-options)))]
    (reagent/create-class
      {:reagent-render (fn []
                         [:div#sessions-chart.chart {:style {:position :initial}}])
       :component-did-mount (fn [component]
                              (let [props (reagent/props component)]
                                (reset! chart-atom (.init js/echarts (.getElementById js/document "sessions-chart"))))
                              (update component))
       :display-name "sessions-chart"
       :component-did-update update})))


(defn country-chart-component [conf]
  (let [chart-atom (reagent/atom nil)
        update (fn [component]
                 (let [props (reagent/props component)
                       data (:data props)
                       map-data (map (fn [item]
                                       (let [result {"name" (helpers/normalize-country-name (:country item)) "value" (:t item)}]
                                         result))
                                     data)
                       values (map (fn [item] (:t item)) data)
                       min-visitors (apply min values)
                       max-visitors (apply max values)

                       labels (map :_id data)
                       total-sessions-data (map :t data)
                       total-visitors-data (map :u data)
                       new-visitors-data (map :n data)

                       options {"title" {
                                         "text" "Visitors by country"
                                         ;subtext: 'from United Nations, Total population, both sexes combined, as of 1 July (thousands) ',
                                         ;sublink: 'http:// esa.un.org / wpp/Excel-Data/population.htm',
                                         "left" "center"
                                         "top" "top"}

                                ;"tooltip" {}
                                "visualMap" {
                                             "min" min-visitors
                                             "max" max-visitors
                                             "text" ["High" "Low"]
                                             "realtime" false
                                             "calculable" true
                                             "inRange" {"color" ["lightskyblue" "yellow" "orangered"]}}

                                "series" [{"name" "Total Sessions"
                                           "mapType" "world"
                                           "type" "map"
                                           ;"roam" true
                                           "itemStyle" {"emphasis" {"label" {"show" true}}}
                                           "data" map-data}]}


                       js-chart-options (clj->js options)
                       _ (.log js/console js-chart-options)]
                   (.setOption @chart-atom js-chart-options)))]
    (reagent/create-class
      {:reagent-render (fn []
                         [:div#country-chart.country-chart {:style {:position :initial}}])
       :component-did-mount (fn [component]
                              (let [props (reagent/props component)]
                                (reset! chart-atom (.init js/echarts (.getElementById js/document "country-chart"))))
                              (update component))
       :display-name "country-chart"
       :component-did-update update})))


(defn dashboard-stats-panel [data]
  (let [dashboard-data (:dashboard data)
        total-sessions-data (:total_sessions dashboard-data)
        total-users-data (:total_users dashboard-data)
        new-users-data (:new_users dashboard-data)
        total-time-data (:total_time dashboard-data)
        avg-time-data (:avg_time dashboard-data)
        avg-requests-data (:avg_requests dashboard-data)

        top (:top data)
        top-platforms (:platforms top)
        top-resolutions (:resolutions top)
        top-users (:users top)
        colors ["red" "green" "blue"]]
    [:div
     [:h2 (:period data)]
     [:ul
      (for [[label data-item] [["Total sessions" total-sessions-data]
                               ["Total users" total-users-data]
                               ["New users" new-users-data]
                               ["Total time" total-time-data]
                               ["Avg time" avg-time-data]
                               ["Avg requests" avg-requests-data]]]
        [:li {:key label}
         [:span (str label " " (:total data-item) " ")]
         [:span
          {:className (str "trend-" (:trend data-item))}
          (:change data-item)]])]

     [:div
      [:h2 "Top:"]
      (for [[label data-item] [["Platforms" top-platforms]
                               ["Resolutions" top-resolutions]
                               ["Users" top-users]]]
        [:div.col-md-4 {:key label}
         (for [[{:keys [name percent]} color] (map vector data-item colors)]
           [:div.tops {:key name
                       :style {:width (str percent "%")
                               :background-color color}}
            (str name " " percent "%")])])]]))


(defn aggregated-stats-charts [funnel-id]
  (let [active-dashboard-tab (reagent/atom :30days)]
    (fn [funnel-id]
      (let [aggregated-stats (re-frame/subscribe [:aggregated-stats funnel-id])
            sessions-stats (:sessions @aggregated-stats)
            country-stats (get-in @aggregated-stats [:country :30days])
            dashboard-stats (:dashboard @aggregated-stats)]
        [:div.row
        ; [:div.col-md-12
        ;  [:ul.nav.nav-tabs
        ;   [:li {:role "presentation" :class (if (= @active-dashboard-tab :30days) "active" "")}
        ;    [:a {:role :button
        ;         :on-click (set-active-tab active-dashboard-tab :30days)} "30 days"]]
        ;
        ;   [:li {:role "presentation" :class (if (= @active-dashboard-tab :7days) "active" "")}
        ;    [:a {:role :button
        ;         :on-click (set-active-tab active-dashboard-tab :7days)} "7 days"]]
        ;
        ;   [:li {:role "presentation" :class (if (= @active-dashboard-tab :today) "active" "")}
        ;    [:a {:role :button
        ;         :on-click (set-active-tab active-dashboard-tab :today)} "Today"]]]]

         [:div.col-md-12
          [:div.panel.panel-default
           [:div.panel-body
            [dashboard-stats-panel (@active-dashboard-tab dashboard-stats)]]]]

         [:div.row
          [:div.col-md-12
           [sessions-chart-component {:data sessions-stats}]]]

         [:div.row
          [:div.col-md-12
           [country-chart-component {:data country-stats}]]]]))))


(defn reports-visitor-stats-panel []
  (let [funnels (re-frame/subscribe [:funnels])
        funnel-list (vals @funnels)
        form-state (reagent/atom {:funnel (:id (first funnel-list))})
        funnel-select-options (mapcat (fn [funnel] [(:id funnel) (:name funnel)])
                                      funnel-list)]
    (fn []
      [:div.row
       [:div.col-md-6
        [:h1 "Visitor stats"]]
       [:div.col-md-6
        [free-form/form
         @form-state
         {}
         (helpers/form-update-handler form-state)
         :bootstrap-3
         [:form
          [:free-form/field {:type :select
                             :key :funnel
                             :label "Funnel"
                             :options funnel-select-options}]]]]
       [aggregated-stats-charts (:funnel @form-state)]])))


;; user products panel
;; ==============================


(defn user-product-main-panel [user-product current-panel]
  (let [phone-numbers (re-frame/subscribe [:phone-numbers])]
    (fn [user-product current-panel]
      (let [phone-number (:number ((keyword (str (:phone_number user-product))) @phone-numbers))]
        [:div.row.user-product-row {:key (:id user-product)}
         [:div.col-md-4
          [:div.user-product-image-wrapper
           [:h2 (:name user-product)]
           [:div
            [:img.user-product-image {:src (:image user-product)}]]
           [:div "Price: "
            [:span.user-product-price (str "$" (:price user-product))]]
           [:div "Support phone: "
            [:span.user-product-price
             (if phone-number
               phone-number
               "Not selected")]]]

          [:a.btn.btn-default {:role :button
                               :on-click (fn [e]
                                           (.preventDefault e)
                                           (reset! current-panel :edit-panel))}
           "Change"]]
         [:div.col-md-8
          [:div.user-product-label-wrapper]
          [:a.btn.btn-default {:href ""} "Design label"]]]))))


(defn user-product-edit-panel [user-product current-panel]
  (let [form-state (reagent/atom user-product)
        form-errors (reagent/atom {})
        ajax-active (reagent/atom false)
        phone-numbers (re-frame/subscribe [:phone-numbers])
        close-edit-form (fn [] (reset! current-panel :main-panel))]
    (fn [user-product current-panel]
      [:div.row.user-product-row {:key (:id @form-state)}
       [:h2 (str "Change product " (:name @form-state))]
       [free-form/form
        @form-state
        @form-errors
        (helpers/form-update-handler form-state)
        :bootstrap-3
        [:form
         [:div.col-md-4
          [:free-form/field {:type :text
                             :key :name
                             :label "Name"}]
          [:free-form/field {:type :text
                             :key :price
                             :label "Price"}]
          [:div.form-group
           [:label "Support Phone"]
           [:select.form-control {:free-form/input {:key :phone_number}}
            [:option {:value ""} "Select phone"]
            (for [phone-number (vals @phone-numbers)]
              [:option {:key (:number phone-number) :value (:id phone-number)} (:number phone-number)])]]
          [:button.btn.btn-primary {:role :button
                                    :on-click (helpers/form-submit-handler
                                                validators/user-product-validator
                                                form-state
                                                form-errors
                                                ajax-active
                                                :update-user-product
                                                close-edit-form)}
           (ajax-loader-or-text ajax-active "Save")]
          [:a.btn {:role :button
                   :on-click close-edit-form}
           "Cancel"]]
         [:div.col-md-8
          [:div.form-group {:free-form/error-class {:key :description :error "has-error"}}
           [:label.control-label "Description"]
           [:div
            [:textarea.form-control
             {:free-form/input {:key :description}

              :rows 12}]
            [:div.text-danger {:free-form/error-message {:key :description}} [:p.error]]]]]]]])))


(defn user-product-row [user-product]
  (let [current-panel (reagent/atom :main-panel)]
    (fn [user-product]
      (case @current-panel
        :main-panel [user-product-main-panel user-product current-panel]
        :edit-panel [user-product-edit-panel user-product current-panel]
        [user-product-main-panel user-product current-panel]))))


(defn user-products-panel []
  (let [user-products (re-frame/subscribe [:user-products])]
    (fn []
      [:div
       [:div.row
        [:div.col-md-12
         [:h1
          "Your products"
          [:a.btn.btn-primary.float-right {:href (resolve-path :select-products-panel)} "Add product"]]]]
       (for [user-product (vals @user-products)]
         [user-product-row user-product])])))


;; select products panel
;; ==============================

(defn product-info-modal [product]
  [:div
   [:div.modal-header
    [:h4.modal-title (:name product)]]
   [:div.modal-body
    [:img.description-product-image {:src (:image product)}]
    [:div {:dangerouslySetInnerHTML {:__html (:description product)}}]]

   [:div.modal-footer
    [:button.btn.btn-success {:type "button"
                              :data-dismiss "modal"
                              :on-click (fn [_] (re-frame/dispatch [:add-user-product product]))}
     "Select"]
    [:button.btn.btn-default {:type "button"
                              :data-dismiss "modal"} "Close"]]])


(defn product-row [product]
  (let [ajax-active (reagent/atom false)
        feedback-channel (chan 1)]
    (fn [product]
      [:div.col-md-4.product {:key (:id product)}
       [:h2 (:name product)]
       [:div.product-image-wrapper
        [:img.product-image {:src (:image product)}]]
       [:div "Price: "
        [:span.product-price (str "$" (:price product))]]
       [:div
        [:a.btn.btn-primary {:role :button
                             :on-click (fn [e]
                                         (.preventDefault e)
                                         (modals/modal! (product-info-modal product)))}

         "Info"]
        [:a.btn.btn-success {:role :button
                             :on-click (fn [_]
                                         (reset! ajax-active true)
                                         (re-frame/dispatch [:add-user-product product feedback-channel])
                                         (go
                                           (<! feedback-channel)
                                           (navigate :user-products-panel)))}
         (ajax-loader-or-text ajax-active "Select")]]])))


(defn select-products-panel []
  (let [products (re-frame/subscribe [:products])]
    [:div
     [:div.row
      [:div.col-md-12
       [:h1 "Select product you want to sell"]]]
     [:div.row
      (for [product (vals @products)]
        [product-row product])]]))


;; user profile
;; =========================================

(defn stripe-account-form [stripe-account]
  (let [stripe-accounts-in-use (re-frame/subscribe [:stripe-accounts-in-use])
        stripe-account-state (reagent/atom stripe-account)
        stripe-account-dirty (reagent/atom false)]
    (fn [_]
      [:div.row {:key (:id @stripe-account-state)}
       [free-form/form
        @stripe-account-state
        {}
        (fn [keys value]
          (reset! stripe-account-dirty true)
          (swap! stripe-account-state assoc-in keys value))
        [:form.padded
         [:div.col-md-3
          [:input.form-control {:free-form/input {:key :name}
                                :type :text}]]
         [:div.col-md-3
          [:input.form-control {:free-form/input {:key :api_key}
                                :type :text}]]

         [:div.col-md-3
          [:input.form-control {:free-form/input {:key :api_secret}
                                :type :text}]]
         [:div.col-md-3
          (if @stripe-account-dirty
            [:a.btn.btn-success.btn-sm {:role :button
                                        :on-click (fn [e]
                                                    (.preventDefault e)
                                                    (reset! stripe-account-dirty false)
                                                    (re-frame/dispatch [:update-stripe-account @stripe-account-state]))}
             "Save"])
          (if (not (contains? (set @stripe-accounts-in-use) (:id @stripe-account-state)))
            [:a.btn.btn-danger.btn-sm {:role :button
                                       :on-click (helpers/dom-event-handler [:delete-stripe-account (:id @stripe-account-state)])} "Delete"]
            [:span "Account in use"])]]]])))


(defn add-stripe-account-form [add-form-visible-atom]
  (let [create-stripe-account-template (reagent/atom {})]

    (fn [add-form-visible-atom]
      [:div.row
       [free-form/form
        @create-stripe-account-template
        {}
        (fn [keys value] (swap! create-stripe-account-template assoc-in keys value))
        [:form.padded
         [:div.col-md-3
          [:input.form-control {:free-form/input {:key :name}
                                :type :text}]]
         [:div.col-md-3
          [:input.form-control {:free-form/input {:key :api_key}
                                :type :text}]]
         [:div.col-md-3
          [:input.form-control {:free-form/input {:key :api_secret}
                                :type :text}]]
         [:div.col-md-3
          [:a.btn.btn-success {:role :button
                               :on-click (fn [e]
                                           (.preventDefault e)
                                           (re-frame/dispatch [:add-stripe-account @create-stripe-account-template])
                                           (reset! add-form-visible-atom false))}
           "Create"]]]]])))


(defn profile-panel []
  (let [form-state (reagent/atom (deref (re-frame/subscribe [:user-profile])))
        form-errors (reagent/atom {})
        ajax-active (reagent/atom false)
        just-saved (reagent/atom false)]
    (fn []
      [:div.row
       [:div.col-md-6.col-md-offset-3
        [:h1 "User profile"]
        [free-form/form
         @form-state
         @form-errors
         (helpers/form-update-handler form-state)
         :bootstrap-3
         [:form
          [:div.row
           [:div.col-md-6
            [:free-form/field {:type :text
                               :key :first_name
                               :label "First Name"}]]
           [:div.col-md-6
            [:free-form/field {:type :text
                               :key :last_name
                               :label "Last Name"}]]]
          [:free-form/field {:type :text
                             :key :company_name
                             :label "Company Name"}]
          [:free-form/field {:type :text
                             :key :company_street_address
                             :label "Company Street Address"}]
          [:div.row
           [:div.col-md-5
            [:free-form/field {:type :text
                               :key :company_city
                               :label "Company City"}]]
           [:div.col-md-4
            [:free-form/field {:type :text
                               :key :company_zipcode
                               :label "Company ZIP Code"}]]
           [:div.col-md-3
            [:free-form/field {:type :text
                               :key :company_state
                               :label "Company State"}]]]
          [:button.btn.btn-success
           {:on-click (helpers/form-submit-handler
                        validators/user-profile-validator
                        form-state
                        form-errors
                        ajax-active
                        :update-user-profile
                        (fn []
                          (reset! ajax-active false)
                          (reset! just-saved true)
                          (js/setTimeout (fn [] (reset! just-saved false)) constants/default-alert-timeout)))}
           (ajax-loader-or-text
             ajax-active
             "Save")]
          (when @just-saved [:a.btn "Saved!"])]]]])))



;; stripe accounts panel
;; ==============================

(defn stripe-accounts-panel []
  (let [stripe-accounts (re-frame/subscribe [:stripe-accounts])
        add-form-visible (reagent/atom false)]
    (fn []
      [:div
       [:div.row
        [:div.col-md-6.col-md-offset-3
         [:h1 "Stripe accounts"]
         [:div.row
          [:div.col-md-3 "Account Name"]
          [:div.col-md-3 "Username"]
          [:div.col-md-3 "Secret"]
          [:div.col-md-3]]

         (for [stripe-account (vals @stripe-accounts)]
           [stripe-account-form stripe-account])
         (when @add-form-visible
           [add-stripe-account-form add-form-visible])
         [:div.row
          [:div.col-md-12
           [:a.btn.btn-primary {:role :button
                                :on-click (fn [e]
                                            (.preventDefault e)
                                            (reset! add-form-visible true))}
            "Add account"]]]]]])))





;; main panel
;; =============

(defn- panels [panel-name]
  (case panel-name
    :home-panel [home-panel]
    :dashboard-panel [dashboard-panel]
    :user-products-panel [user-products-panel]
    :funnels-panel [funnels-panel]
    :create-funnel-panel [create-funnel-panel]
    :reports-call-log-panel [reports-call-log-panel]
    :reports-visitor-stats-panel [reports-visitor-stats-panel]
    :phones-panel [phones-panel]
    :new-phone-number-panel [new-phone-number-panel]
    :domains-panel [domains-panel]
    :new-domain-panel [new-domain-panel]
    :select-products-panel [select-products-panel]
    :profile-panel [profile-panel]
    :stripe-accounts-panel [stripe-accounts-panel]
    [:div]))


(defn show-panel [panel-name]
  [panels panel-name])


(defn main-panel []
  (let [active-panel (re-frame/subscribe [:active-panel])]
    (fn []
      [:div
       [navbar]
       [:div.container
        [show-panel @active-panel]]
       [modals/modal-window]])))

