(ns js-app.routes
  (:require [bide.core :as bide]
            [re-frame.core :as re-frame]))

(def router
  (bide/router
    [["/" :home-panel]
     ["/dashboard" :dashboard-panel]
     ["/products/select" :select-products-panel]
     ["/products" :user-products-panel]
     ["/funnels" :funnels-panel]
     ["/funnels/create" :create-funnel-panel]
     ["/reports/call-log" :reports-call-log-panel]
     ["/reports/visitor-stats" :reports-visitor-stats-panel]
     ["/phones" :phones-panel]
     ["/phones/new" :new-phone-number-panel]
     ["/domains" :domains-panel]
     ["/domains/new" :new-domain-panel]
     ["/profile" :profile-panel]
     ["/profile/stripe-accounts" :stripe-accounts-panel]]))


(defn on-navigate
  "A function which will be called on each route change."
  [name params query]
  (re-frame/dispatch [:set-active-panel name]))


(defn resolve-path [name]
  (str "#" (bide/resolve router name)))

(defn navigate [id]
  (bide/navigate! router id))

(defn start-navigation []
  (bide/start! router {:default :home-panel
                       :on-navigate on-navigate}))
