(ns js-app.core
  (:require [reagent.core :as reagent]
            [day8.re-frame.http-fx]
            [re-frame.core :as re-frame]
            [re-frisk.core :refer [enable-re-frisk!]]
            [js-app.global_events]
            [js-app.subs]
            [js-app.routes :as routes]
            [js-app.views :as views]
            [js-app.config :as config]))


(defn dev-setup []
  (when config/debug?
    (enable-console-print!)
    (enable-re-frisk!)
    (println "dev mode")))

(defn mount-root []
  (re-frame/clear-subscription-cache!)
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn ^:export init []
  (routes/start-navigation)
  (re-frame/dispatch-sync [:initialize-db])
  (dev-setup)
  (mount-root))
