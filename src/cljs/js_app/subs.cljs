(ns js-app.subs
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require [re-frame.core :as re-frame]
            [reagent.ratom :refer [make-reaction]]
            [js-app.helpers :as helpers]
            [ajax.core :as ajax]
            [js-app.routes :as routes]))

;:user-profile
(re-frame/reg-sub
  :user-profile
  (fn [db]
    (:user-profile db)))

;:support-phone-area-codes
(re-frame/reg-sub
  :support-phone-area-codes
  (fn [db]
    (:support-phone-area-codes db)))

;:products
(re-frame/reg-sub
  :products
  (fn [db]
    ;(let [selected-products (set (map :product (:user-products db)))]
    (:products db)))

;:user-products
(re-frame/reg-sub
  :user-products
  (fn [db]
    (:user-products db)))

;:user-product
(re-frame/reg-sub
  :user-product
  (fn [db [_ product-id]]
    (let [user-products (:user-products db)]
      ((helpers/int->keyword product-id) user-products))))

;:primary-product
(re-frame/reg-sub
  :primary-product
  (fn [db [_ funnel-id]]
    (let [primary-product-id (->> db
                                  :funnels
                                  ((helpers/int->keyword funnel-id))
                                  :primary_product)
          result (->> db
                      :user-products
                      ((helpers/int->keyword primary-product-id)))]
      result)))

;:product-packages
(re-frame/reg-sub
  :product-packages
  (fn [db [_ funnel-id user-product-id]]
    (filter (fn [product-package]
              (and (= funnel-id (:funnel product-package))
                   (= user-product-id (:user_product product-package))))
            (vals (:product-packages db)))))

;:active-panel
(re-frame/reg-sub
  :active-panel
  (fn [db _]
    (:active-panel db)))

;:stripe-accounts
(re-frame/reg-sub
  :stripe-accounts
  (fn [db _]
    (:stripe-accounts db)))

;:stripe-account
(re-frame/reg-sub
  :stripe-account
  (fn [db [_ account-id]]
    (->> db
         :stripe-accounts
         account-id)))

;:funnels
(re-frame/reg-sub
  :funnels
  (fn [db _]
    (:funnels db)))

;:funnel
(re-frame/reg-sub
  :funnel
  (fn [db [_ funnel-id]]
    (->> db
         :funnels
         ((helpers/int->keyword funnel-id)))))

;:domains
(re-frame/reg-sub
  :domains
  (fn [db _]
    (let [funnels (re-frame/subscribe [:funnels])
          groupped-funnels (group-by :domain (vals @funnels))
          domains (:domains db)
          domains-annotated (map
                              (fn [domain] (assoc domain :funnels (get groupped-funnels (:id domain))))
                              (vals domains))]
      domains-annotated)))

;:stripe-accounts-in-use
(re-frame/reg-sub
  :stripe-accounts-in-use
  (fn [db _]
    (let [funnels (vals (:funnels db))
          stripe-accounts (map :stripe_account funnels)]
      stripe-accounts)))

;:support-phones
(re-frame/reg-sub
  :support-phones
  (fn [db _]
    (:support-phones db)))

;:support-phones-loading
(re-frame/reg-sub
  :support-phones-loading
  (fn [db _]
    (:support-phones-loading db)))

;:call-log
(re-frame/reg-sub-raw
  :call-log
  (fn [app-db _]
    (let [query-token (do
                        (ajax/ajax-request {:uri             "/api/v1/funnels/call_log/"
                                            :method          :get
                                            :format          (ajax/url-request-format)
                                            :response-format (ajax/json-response-format {:keywords? true})
                                            :headers         {"X-CSRFToken" (:csrf @app-db)}
                                            :handler         (fn [[ok response]]
                                                               (if ok
                                                                 (re-frame/dispatch [:call-log-loaded response])))}))]
      (make-reaction
        (fn [] (:call-log @app-db))
        :on-dispose #(re-frame/dispatch [:clean-call-log])))))


;:visitor-stats
(re-frame/reg-sub-raw
  :visitor-stats
  (fn [app-db [_ funnel-id]]
    (let [query-token (do
                        (ajax/ajax-request {:uri             (str "/api/v1/analytics/sessions/" funnel-id "/")
                                            :method          :get
                                            :format          (ajax/url-request-format)
                                            :response-format (ajax/json-response-format {:keywords? true})
                                            :headers         {"X-CSRFToken" (:csrf @app-db)}
                                            :handler         (fn [[ok response]]
                                                               (if ok
                                                                 (re-frame/dispatch [:visitor-stats-loaded funnel-id response])))}))]
      (make-reaction
        (fn [] (->> @app-db
                    :visitor-stats
                    ((helpers/int->keyword funnel-id))))
        :on-dispose #(re-frame/dispatch [:clean-visitor-stats funnel-id])))))


;:aggregated-stats
(re-frame/reg-sub-raw
  :aggregated-stats
  (fn [app-db [_ funnel-id]]
    (let [query-token (do
                        (ajax/ajax-request {:uri             (str "/api/v1/analytics/aggregated/" funnel-id "/")
                                            :method          :get
                                            :format          (ajax/url-request-format)
                                            :response-format (ajax/json-response-format {:keywords? true})
                                            :headers         {"X-CSRFToken" (:csrf @app-db)}
                                            :handler         (fn [[ok response]]
                                                               (if ok
                                                                 (re-frame/dispatch [:aggregated-stats-loaded funnel-id response])))}))]
      (make-reaction
        (fn [] (->> @app-db
                    :aggregated-stats
                    ((helpers/int->keyword funnel-id))))
        :on-dispose #(re-frame/dispatch [:clean-aggregated-stats funnel-id])))))


;:phone-numbers
(re-frame/reg-sub
  :phone-numbers
  (fn [db _]
    (:phone-numbers db)))


;:phone-numbers-annotated
(re-frame/reg-sub
  :phone-numbers-annotated
  (fn [db _]
    (let [phone-numbers (:phone-numbers db)
          user-products (:user-products db)
          groupped-user-products (group-by :phone_number (vals user-products))
          phone-numbers-annotated (map
                                    (fn [phone-number] (assoc phone-number :user-products (get groupped-user-products (:id phone-number))))
                                    (vals phone-numbers))]
      phone-numbers-annotated)))


;:phone-number
(re-frame/reg-sub
  :phone-number
  (fn [db [_ phone-id]]
    (->> db
         :phone-numbers
         ((helpers/int->keyword phone-id)))))


;:upsell-packages
(re-frame/reg-sub
  :upsell-packages
  (fn [db [_ funnel-id]]
    (let [all-upsell-packages (vals (:upsell-packages db))
          filtered-packages (filter (fn [{f-id :funnel}] (= f-id funnel-id)) all-upsell-packages)
          ordered-packages (sort-by :order filtered-packages)]
      ordered-packages)))


;:default-greeting
(re-frame/reg-sub
  :default-greeting
  (fn [db [_]]
    (:default-greeting db)))


;:funnel-templates
(re-frame/reg-sub
  :funnel-templates
  (fn [db [_]]
    (:funnel-templates db)))

;:funnel-template
(re-frame/reg-sub
  :funnel-template
  (fn [db [_ template-id]]
    (->> db :funnel-templates ((helpers/int->keyword template-id)))))

;:global-issues
(re-frame/reg-sub
  :global-issues
  (fn [db [_]]
    (let [count-objects (fn [obj-key] (= (count (vals (obj-key db))) 0))
          no-phone-numbers (count-objects :phone-numbers)
          no-phone-numbers-message "You need at least one phone number"
          no-phone-numbers-url (routes/resolve-path :new-phone-number-panel)

          no-stripe-account (count-objects :stripe-accounts)
          no-stripe-account-message "You need to enter at least one Stripe account to accept payments"
          no-stripe-account-url (routes/resolve-path :stripe-accounts-panel)

          no-domain (count-objects :domains)
          no-domain-message "You need at least one domain for your sites"
          no-domain-url (routes/resolve-path :new-domain-panel)

          no-user-products (count-objects :user-products)
          no-user-products-message "You need to select at least one product to sell"
          no-user-products-url (routes/resolve-path :select-products-panel)

          issues [[no-phone-numbers no-phone-numbers-message no-phone-numbers-url]
                  [no-stripe-account no-stripe-account-message no-stripe-account-url]
                  [no-domain no-domain-message no-domain-url]
                  [no-user-products no-user-products-message no-user-products-url]]
          active-issues (filter (fn [[no-something _ _]] no-something)
                                issues)]
      active-issues)))
