(ns js-app.validators
  (:require [validateur.validation :as v]))

(def package-validator
  (v/validation-set
    ;; todo: validate correct numbers
    (v/presence-of :quantity)
    (v/presence-of :price)))


(def upsell-package-validator
  (v/validation-set
    ;; todo: validate correct numbers
    (v/presence-of :user_product)
    (v/presence-of :quantity)
    (v/presence-of :price)))


(def user-product-validator
  ;; todo: validate numbers
  (v/validation-set
    (v/presence-of :name)
    (v/presence-of :price)))


(def primary-product-validator
  (v/validation-set
    (v/presence-of :primary_product)))


(def phone-number-validator
  (v/validation-set
    (v/presence-of :greeting)))


(def new-phone-number-validator
  (v/validation-set
    (v/presence-of :number)
    (v/presence-of :greeting)))


(def new-domain-name-validator
  (v/validation-set
    (v/presence-of :domain)))


(def new-funnel-validator
  (v/validation-set
    (v/presence-of :name)
    (v/presence-of :primary_product)
    (v/presence-of :template)))

(def funnel-path-validator
  (v/validation-set))

(def funnel-stripe-account-validator
  (v/validation-set
    (v/presence-of :name)))

(def user-profile-validator
  (v/validation-set))
