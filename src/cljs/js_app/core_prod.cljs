(ns js-app.core-prod
  (:require [reagent.core :as reagent]
            [day8.re-frame.http-fx]
            [re-frame.core :as re-frame]
            [js-app.global_events]
            [js-app.subs]
            [js-app.routes :as routes]
            [js-app.views :as views]
            [js-app.config :as config]))


(defn mount-root []
  (re-frame/clear-subscription-cache!)
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn ^:export init []
  (.log js/console "prod mode")
  (routes/start-navigation)
  (re-frame/dispatch-sync [:initialize-db])
  (mount-root))
