(ns js-app.helpers
  (:require [cljs.core.async :as async]
            [ajax.core :as ajax]
            [re-frame.core :as re-frame]
            [cljs.core.async :refer [<! >! close! chan]])
  (:require-macros
    [cljs.core.async.macros :refer [go]]))


(defn dom-event-handler [args]
  (fn [e]
    (do (.preventDefault e)
        (re-frame/dispatch args))))


(defn int->keyword [i]
  (-> i
      (str)
      (keyword)))


(defn form-update-handler
  ([form-state] (form-update-handler form-state nil))
  ([form-state debug]
   (fn [keys value]
     (when debug
       (println keys value))
     (swap! form-state assoc-in keys value))))


(defn form-submit-handler
  ([validator form-state form-errors ajax-active event-name]
   (form-submit-handler validator form-state form-errors ajax-active event-name nil))
  ([validator form-state form-errors ajax-active event-name success-callback]
   (fn [e]
     (.preventDefault e)
     ;; first validate broser-side
     (let [validation-result (validator @form-state)]
       (if (not (= {} validation-result))
         ;; show errors
         (reset! form-errors validation-result)
         (let [feedback-channel (chan 1)]
           ;; otherwise do ajax call
           (reset! ajax-active true)
           (re-frame/dispatch [event-name @form-state feedback-channel])
           (go
             ;; ... and wait for result from channel
             (let [[feedback-status data] (<! feedback-channel)]
               (reset! ajax-active nil)
               (if feedback-status
                 ;; if success
                 (do
                   (reset! form-errors {})
                   (if success-callback
                     (success-callback)))
                 ;; othewise show errors
                 (reset! form-errors data))))))))))


(defn map-keys-to-string [data]
  (zipmap (map name (keys data)) (vals data)))


(defn init-map [keys]
  (reduce (fn [m k] (assoc m k nil)) {} keys))


(defn respond-to-channel [channel data]
  (go
    (>! channel data)
    (close! channel)))


(defn normalize-country-name [name]
  (case name
    "United States" "United States of America"
    name))
