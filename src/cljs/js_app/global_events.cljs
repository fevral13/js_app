(ns js-app.global_events
  (:require [re-frame.core :as re-frame]
            [goog.net.cookies]
            [js-app.events.profile-edit]
            [js-app.events.user-products]
            [js-app.events.funnels]
            [js-app.events.product-packages]
            [js-app.events.upsell-packages]
            [js-app.events.call-log]
            [js-app.events.phone-numbers]
            [js-app.events.stats]
            [js-app.events.domains]
            [js-app.helpers :as helpers]))

;; :initialize-db
(re-frame/reg-event-db
  :initialize-db
  (fn [_ _]
    (->
      ; get data from INITIAL global js var
      (js->clj js/INITIAL :keywordize-keys true)
      ; put CSRF token to app state to be used by ajax calls
      (assoc :csrf (.get goog.net.cookies "csrftoken")))))


;; :set-active-panel
(re-frame/reg-event-db
  :set-active-panel
  (fn [db [_ active-panel]]
    (assoc db :active-panel active-panel)))


;; :ajax-call-fail
;; generic event handler, accepts feedback channel to send fail event to and raw server response
(re-frame/reg-event-fx
  :ajax-call-fail
  (fn [_ [_ feedback-channel {:keys [response]}]]
    (helpers/respond-to-channel feedback-channel [false response])
    {}))
