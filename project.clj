(defproject js-app "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.9.521"]
                 [org.clojure/core.async "0.3.442"]
                 [reagent "0.6.1"]
                 [re-frame "0.9.2"]
                 [re-frisk "0.4.4"]
                 [funcool/bide "1.4.0"]
                 [cljs-ajax "0.5.9"]
                 [org.clojars.frozenlock/reagent-modals "0.2.6"]
                 [day8.re-frame/http-fx "0.1.3"]
                 [spellhouse/clairvoyant "0.0-72-g15e1e44"]
                 [com.novemberain/validateur "2.5.0"]]

  :plugins [[lein-cljsbuild "1.1.4"]]

  :min-lein-version "2.5.3"

  :source-paths ["src/clj" "lib/free-form/src/cljs"]

  :clean-targets ^{:protect false} ["resources/public/js/compiled"
                                    "target"
                                    "test/js"
                                    "../static-local/js/compiled"
                                    "../static-local/js/app.js"
                                    "../static-local/js/app.min.js"
                                    "../static-local/js/compiled.min"]

  :figwheel {:css-dirs ["../static-local/css"]}

  :profiles
  {:dev
   {:dependencies [[binaryage/devtools "0.9.3"]
                   [figwheel-sidecar "0.5.10"]]

    :plugins [[lein-figwheel "0.5.10"]
              [lein-doo "0.1.7"]
              [lein-ancient "0.6.10"]]}}

  :cljsbuild
    {:builds
     [{:id "dev"
       :source-paths ["src/cljs" "lib/free-form/src/cljs"]
       :figwheel {:on-jsload "js_app.core/mount-root"}
       :compiler {:main js_app.core
                  :output-to "../static-local/js/compiled/app.js"
                  :output-dir "../static-local/js/compiled"
                  :asset-path "/static/js/compiled"
                  :source-map-timestamp true
                  :closure-defines {goog.DEBUG true}
                  :pretty-print true
                  :preloads [devtools.preload]
                  :external-config {:devtools/config {:features-to-install :all}}}}

      {:id "min"
       :source-paths ["src/cljs" "lib/free-form/src/cljs"]
       :compiler {:main js_app.core-prod
                  :output-to "../static-local/js/app.min.js"
                  :output-dir "../static-local/js/compiled.min"
                  :optimizations :advanced
                  :closure-defines {goog.DEBUG false}
                  :externs ["externs/echarts.js"]
                  :parallel-build true
                  :pretty-print false}}

      {:id "test"
       :source-paths ["src/cljs" "test/cljs"]
       :compiler {:main js_app.runner
                  :output-to "resources/public/js/compiled/test.js"
                  :output-dir "resources/public/js/compiled/test/out"
                  :optimizations :none}}]})
