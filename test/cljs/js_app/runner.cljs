(ns js-app.runner
    (:require [doo.runner :refer-macros [doo-tests]]
              [js-app.core-test]))

(doo-tests 'js-app.core-test)
